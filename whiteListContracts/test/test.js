/*global artifacts*/
/*global contract*/
/*global web3*/
var Whitelist = artifacts.require("Whitelist");
var assert = require('assert');

function to(promise) {  
   return promise.then(data => {
      return [null, data];
   })
   .catch(err => [err]);
}
  
contract('Async Whitelist', function (accounts) {
  
  
  it("should not whitelist random addresses", async function () {
    let instance = await Whitelist.deployed();
    var expectedWhitelistStatus = false;
    var testAddress = web3.eth.accounts[1];
    let whitelistStatus = await instance.userAddr(testAddress);

    assert.equal(whitelistStatus, expectedWhitelistStatus, "address is not whitelisted before add +whitelist");
  });
  
  it("should not whitelist newly added addresses", async function () {
    let instance = await Whitelist.deployed();
    var expectedWhitelistStatus = false;
    var testAddress = web3.eth.accounts[1];
    
    await instance.addAddress(testAddress);
    let whitelistStatus = await instance.userAddr(testAddress);
    
    assert.equal(whitelistStatus, expectedWhitelistStatus, "address is not whitelisted before add +whitelist");
  });
  
  it("should whitelist address whitelisted by owner", async function () {
    let instance = await Whitelist.deployed();
    var expectedWhitelistStatus = true;
    var testAddress = web3.eth.accounts[1];
    
    await instance.addAddress(testAddress);
    await instance.whitelistAddress(testAddress);
    let whitelistStatus = await instance.userAddr(testAddress);
    
    assert.equal(whitelistStatus, expectedWhitelistStatus, "address is whitelisted after add +whitelist");
  });
  
    it("should not whitelist address whitelisted by not owner", async function () {
    let instance = await Whitelist.deployed();
    var expectedWhitelistStatus = true;
    var testAddress = web3.eth.accounts[1];
    
    await instance.addAddress(testAddress);
    let err = await to(instance.whitelistAddress(testAddress,{from:web3.eth.accounts[2]}))
    if (err == null){assert.fail(null,"throw", "should throw when not called by owner, transaction was not reverted")};
    let whitelistStatus = await instance.userAddr(testAddress);
    
    assert.notEqual(whitelistStatus, expectedWhitelistStatus, "address is whitelisted after add +whitelist");
  });
  
});