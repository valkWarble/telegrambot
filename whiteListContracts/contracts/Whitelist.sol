pragma solidity ^0.4.17;
contract Owned {
    address public owner;
    function   Owned() internal{
        owner = msg.sender;
    }
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
}
contract Whitelist is Owned {
    mapping (address => bool) public userAddr;
    
    function  whitelistAddress (address user) external onlyOwner {
        userAddr[user] = true;
    }
    function  addAddress (address user) external{
        userAddr[user] = false;
    }
}