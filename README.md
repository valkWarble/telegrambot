

This chat example showcases how to use `socket.io` with a static `express` server.

## Running the server

1) Open `server.js` and start the app by clicking on the "Run" button in the top menu.

2) Set Environment variables:

    $ set TELEGRAM_BOT_TOKEN=494965164:AAGN5b4rlUpMNiH7nDH98TzkUMwNkn7vigo
    $ set TELEGRAM_BOT_NAME=eximchainDEVONLYBOT
    $ set TELEGRAM_AUTHORIZATION_SALT="*&^$#*YHDIUIYR(Q*ud ciusayr98we&*#^$*"

    //Set url and port, globally
    $ set PARTNER_API_HOST=54.144.78.241
    $ set PARTNER_API_PORT=12001

    //Set url and port, command specific
    $ set PARTNER_API_submitDocument_HOST=34.238.114.187
    $ set PARTNER_API_submitDocument_PORT=11001

3) Generate the SALTED HASH for password and set Environment Variable

    //cd to folder in which authorize.js is present
    $ node
    > let auth = require("./authorize");
    > auth.generate_salted_hash(admins_telegram_user_name, new_pass_phrase)
    //copy the returned pass_phrase, say below line is the pass_phrase
    //95661925b4cb97b978fce56331aeaafe001a400e59d1d411c2599f193dc6d2c5c587c02beb238604a377155f8078f0b25b53ce603fffc041e3183311581563df

    $ set TELEGRAM_AUTHORIZATION_SALTED_HASH=95661925b4cb97b978fce56331aeaafe001a400e59d1d411c2599f193dc6d2c5c587c02beb238604a377155f8078f0b25b53ce603fffc041e3183311581563df


3) Alternatively you can launch the app from the Terminal:

    $ node server.js
