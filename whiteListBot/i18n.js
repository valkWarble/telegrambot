/**
 * @member {Object<String, String>}
 * resource strings will be added here
 * */
const resources = {
    "test.key.1": "test key one",
    "test.key.hollow": "test key {0}",

    "generic.error": "Whoa! Something has gone wrong.",

    "address.for.add": "Let me know your ETH address so that I can add.",
    "address.added": "Address {0} added to the list successfully.",
    "authenticate.provide.pass_phrase": "Hello there admin! Whats the pass-phrase?",
    "authentication.failed": "Looks like you are not an Administrator! If so, please retry.",
    "authentication.success": "Authenticated Successfully!",
    "address.for.approve": "Let me know which ETH address you need to approve.",
    "recommend.message.delete": "I recommend you to delete the message where in you sent the pass-phrase",
    "address.whitelisting.success": "Address {0} whitelisted successfully.",
    "address.for.check": "Let me know your ETH address so that I can check.",
    "address.check.true": "Yes, {0} is whitelisted.",
    "address.check.false": "Nope. {0} address is  not whitelisted.",
    "eth.invalid.address.provided": "Oops! Looks like the address ain't not valid. Provide a valid address",
    "error.occurred": "Error Occurred! {0}",
    "failed.whitelist.check": "Unable to check whitelist.",
    "failed.kyc.add": "Unable to add {0} to list.",
    "failed.whitelist.address": "Unable to approve {0} to whitelist",
    "user.approval.status": "Your document approval status: {0}",
    "user.approval.not_submitted": "Looks like you haven't submitted the document yet!",
    "upload.document": "Please upload the document",
    "invalid.document": "Looks like this is not the requested document. Please upload a document!",
    "document.uploaded": "Document received. Will be processed.",
    "api.failure.check_status": "Sorry! Unable to check status",
    "api.failure.upload_document": "Sorry! Unable to upload document",
    "api.failure.upload_individual": "Sorry! Unable to upload information",
    "information.submitted": "Data recorded successfully. Your approval status: {0}. Please use /{0} command to check your status",

    "info.field.first_name": "First Name",
    "info.field.last_name": "Last Name",
    "info.field.country_of_residence": "Country of Residence",
    "info.field.nationality": "Nationality",
    "info.field.ssic_code": "Industry Code",
    "info.field.ssoc_code": "Occupaion Code",
    "info.field.onboarding_mode": "Onboarding Mode",
    "info.field.payment_mode": "Payment Mode",
    "info.field.product_service_complexity": "Product Service Complexity",

    "info.questionnaire.first_name": "Lets start with your personal details. What's your first name?",
    "info.questionnaire.last_name": "And your last?",
    "info.questionnaire.country_of_residence": "Which country are you from?",
    "info.questionnaire.nationality": "Whats your nationality?",
    "info.questionnaire.ssic_code": "Whats your Industry code?",
    "info.questionnaire.ssoc_code": "Whats your Occupation code",
    "info.questionnaire.onboarding_mode": "How did you get OnBoard?",
    "info.questionnaire.payment_mode": "What is your payment mode?",
    "info.questionnaire.product_service_complexity": "How would you rate your Product Service Complexity?",

    "info.invalid_input.string": "Please provide a valid input",
    "info.invalid_input.choice": "Please choose from one of the provided choices",
    "info.invalid_input.choice.inline_query": "Invalid input. Please use the query and select your choice.",
    "info.invalid_input.http_error": "{0} for field {1}",

    "info.enum.inline_query": "You can query the {0} list by typing `@{1} {2} <your_query_text>`"
};

/**
 * @function
 * @param {String} key - resource key
 * @param {Array<String> = null} substitutes - list if substitutes for the resource keys
 * @description queries the resource keys, forms a message and returns it...
 * */
module.exports = function(key, substitutes){
    "use strict";
    let text = resources[key];
    if(substitutes && substitutes.length){
        for(let i=0;i< substitutes.length;i++){
            text = text.replace( new RegExp( "\\{"+i+"\\}","g") , substitutes[i] || "");
        }
    }
    return text;
};