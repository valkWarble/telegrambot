const ethereum_address = require('ethereum-address');
const ethereum_connector = require('./connector');

let ETHError = ethereum_connector.ETHError;

/**
 * @class
 * @classdesc
 * ETH module which can validate input and forward the required information to EthConnector or throw error
 * */
class ETH{

    /**
     * @constructor
     * @description
     * initializes an isntance of ETH connector form connector module
     * */
    constructor(){
        "use strict";
        this.EthConnector = new ethereum_connector.Connector();
    }

    /**
     * @function
     * @param {String} address - eth address
     * @returns {Boolean} whether the address is a valid eth address or not
     * */
    _ethAddressValidityCheck(address){
        "use strict";
        if (ethereum_address.isAddress(address)) {
            return true;
        } else {
            throw new ETHError("eth.invalid.address.provided");
        }
    }

    /**
     * @function
     * @param {String} address - eth address
     * @param {Function} callback - will be called after successfully adding address to contract
     * */
    addAddress(address, callback){
        "use strict";
        this._ethAddressValidityCheck(address);
        this.EthConnector.addToWhitelist(address, callback);
    };

    /**
     * @function
     * @param {String} address - eth address
     * @param {Function} callback - will be called after successfully whitelisting the address in the contract
     * */
    whitelistAddress(address, callback){
        "use strict";
        this._ethAddressValidityCheck(address);
        this.EthConnector.approveWhitelist(address, callback);
    };

    /**
     * @function
     * @param {String} address - eth address
     * @param {Function} callback - will be called after successfully checking existence of the address in the contract
     * */
    isWhitelisted(address, callback){
        "use strict";
        this._ethAddressValidityCheck(address);
        this.EthConnector.isWhitelisted(address, callback);
    };

}

/**
 * @module
 * */
ETHInterface = {
    ETH : ETH,
    ETHError : ETHError
};

module.exports = ETHInterface;