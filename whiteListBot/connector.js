const conf = require("./conf");
let fs = require("fs");
const Web3 = require("web3");
const i18n = require("./i18n");

const web3 = new Web3(new Web3.providers.HttpProvider(conf.ethereum.provider));

/**
 * @class
 * @classdesc
 * ETH error. will be thrown from Connector
 * */
class ETHError extends Error{
    constructor(message, extra){
        "use strict";
        super(message, extra);
    }
}

/**
 * @class
 * @classdesc
 * ETH Connector, will be used to interact with the smart contract
 * */
class Connector{

    /**
     * @constructor
     * @description
     * Read's the contract schema which is built using truffle migrate and creates a contract instance which can be then interacted with.
     * */
    constructor(){
        "use strict";
        try{
            let data = fs.readFileSync(conf.ethereum.whitelist_contract.build_path);
            let contract_schema = JSON.parse(data);
            this.contract = new web3.eth.Contract(contract_schema.abi, conf.ethereum.whitelist_contract.contract_address);
        }catch (err) {
            this.contract =null;
            console.log(err);
        }
    }

    /**
     * @function
     * @param {Error} err
     * @param {String} msg
     * @description
     * reads the error instance and message string and creates an eth error and throws it.
     * */
    _handleError(err, msg){
        "use strict";
        console.log( (msg?msg:"Error")+"::", err);
        throw new ETHError(i18n("error.occurred", [i18n(msg)]));
    }

    /**
     * @function
     * @param {String} address
     * @param {Function} callback
     * @description
     * calls the smartcontract's addAddress method, and on successful transaction, will call the callback with a reciept
     * */
    addToWhitelist(address, callback){
        "use strict";
        this.contract.methods
            .addAddress(address)
            .send({
                from:conf.ethereum.whitelist_contract.owner_address
            })
            .on('receipt', (receipt) => {
                // console.log('receipt', receipt); //TODO
                callback(receipt);
            })
            .on('error', (err)=>{
                this._handleError(err, i18n("failed.kyc.add", [address]));
            });
    }

    /**
     * @function
     * @param {String} address
     * @param {Function} callback
     * @description
     * calls the smartcontract's whitelistAddress method, and on successful transaction, will call the callback with a reciept
     * */
    approveWhitelist(address, callback){  //sample: "0x4bdf20dfa3ccd75a9438029f3a9bd4cd6c773651"
        "use strict";
        this.contract.methods
            .whitelistAddress(address)
            .send({
                from:conf.ethereum.whitelist_contract.owner_address
            })
            .on('receipt', (receipt) => {
                // console.log(receipt); //TODO
                callback(receipt);
            })
            .on('error', (err)=>{
                this._handleError(err, i18n("failed.whitelist.address", [address]));
            });
    }

    /**
     * @function
     * @param {String} address
     * @param {Function} callback
     * @description
     * calls the smartcontract's suerAddr method, and on successful transaction, will call the callback with a reciept
     * */
    isWhitelisted(address, callback){
        "use strict";
        this.contract.methods
            .userAddr(address)
            .call({
            }, (err, data) => {
                if(err){
                    this._handleError(err, "failed.whitelist.check");
                }else {
                    callback(data);
                }
            });
    }

}

/**
 * @module
 * */
module.exports = {
    Connector : Connector,
    ETHError : ETHError
};