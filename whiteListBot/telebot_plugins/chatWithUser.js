/*
    Get direct answers from user.
*/

/**
 * @member {Object<Number, String>}
 * @description stores contexts of previous message and DOESN't removes it when calling an 'ask.' event listener
 * */
const userPrevContextList = {};

/**
 * @member {Object<Number, String>}
 * @description stores contexts of previous message and removes it when calling an 'ask.' event listener
 * */
const userContextList = {};

/**
 * @member {Object<Number, *>}
 * @description stores conversation / users intent against chat id
 * */
const conversation = {};

/**
 * @function
 * @param {Object} object - object from which query is to be executed
 * @param {String} path - path to be queried for from the object
 * @returns {*} value for the path in object
 * */
function getValueFromJSON(object, path){
    try{
        if(!path){
            return object;
        }
        let keys = path.split('.');
        for(let i=0; i<keys.length; i++){
            object = object[keys[i]];
        }
        return object;
    }catch (e) {
        return undefined;
    }
}

/**
 * @function
 * @param {Object} object - object which is to be updated
 * @param {String} path - path in which the object is to be updated
 * @param {*} value - value to be set against the path in object
 * @description
 * updates the object with new value at the provided path
 * */
function setValueToJson(object, path, value){
    let keys = path.split('.');
    for(let i=0; i<keys.length; i++){
        let key = keys[i];
        if(i+1 < keys.length){
            if(!object.hasOwnProperty(key)){
                object[key]={}
            }
            object=object[key];
        }else{
            object[key]=value
        }
    }
}

/**
 * @class
 * @classdesc
 * user intent interaction layer
 * */
class Intent{

    /**
     * @constructor
     * @param {Number} chat_id - hat id of the user whose intent class to be created
     * */
    constructor(chat_id){
        this.user_id = chat_id;
    }

    /**
     * @function
     * @param {String} key - path as a string separated by dot ( . ) ex: `data.first_name`
     * @param {*} content - content to be stored against the path
     * */
    setContent(key, content){
        "use strict";
        setValueToJson(conversation, this.user_id+"."+key, content);
    }
    /**
     * @function
     * @returns {Boolean} whether content is available or not
     * */
    hasContent(){
        "use strict";
        return conversation.hasOwnProperty(this.user_id);
    }
    /**
     * @function
     * @param {String} key - path as a string separated by dot ( . ) ex: `data.first_name`
     * @returns {*} the content for the provided path
     * */
    getContent(key){
        "use strict";
        return getValueFromJSON(conversation, this.user_id+( key? ( "."+key ) : "" ));
    }
    /**
     * @function
     * @description clears the content
     * */
    clearContent(){
        "use strict";
        delete conversation[this.user_id];
    }
}

module.exports = {

    id: 'chatWithUser',

    /**
     * @function
     * @param {Object} bot - telebot instance
     * @param {Object} msg - telebot message
     * @param {Number} chat_id - chat id of the user
     * @param {Object} props - message properties
     * @description calls the `ask.` listeners if context criteria is met.
     * */
    _fireEvent(bot, msg, chat_id, props){
        const id = chat_id;
        if(props.type === "command"){
            delete userContextList[id];
            (new Intent(id)).clearContent();
            return;
        }
        const context = userContextList[id];
        // If no question, then it's a regular message
        if (!context) return;
        // Delete user from list and send custom event
        userPrevContextList[id] = userContextList[id];
        delete userContextList[id];
        msg.intent = new Intent(id);
        bot.event('ask.' + context, msg, props);
    },

    /**
     * @function
     * @param {Object} bot - telebot instance
     * @description telebot attaches this plugin and can use upon request
     * */
    plugin(bot) {

        // On every message
        bot.on('*', (msg, props) => module.exports._fireEvent(bot, msg, msg.chat.id, props));

        bot.on("callbackQuery", (msg, props) => module.exports._fireEvent(bot, msg, msg.message.chat.id, props));

        // Before call sendMessage method
        bot.on('sendMessage', (args) => {
            const id = args[0];
            const opt = args[2] || {};
            const context = opt.ask;
            // If "ask" in options, add user to list
            if(userPrevContextList[id] !== context){
                (new Intent(id)).clearContent();
            }
            if (context) userContextList[id] = context;

        });

    }
};
