/*
    Get direct answers from user.
*/

const userContextList = {};
const conversation = {};

function getValueFromJSON(object, path){
    try{
        if(!path){
            return object;
        }
        let keys = path.split('.');
        for(let i=0; i<keys.length; i++){
            object = object[keys[i]];
        }
        return object;
    }catch (e) {
        return undefined;
    }
}

function setValueToJson(object, path, value){
    let keys = path.split('.');
    sdpEach(keys, function(i, key){
        if(i+1 < keys.length){
            if(!object.hasOwnProperty(key)){
                object[key]={}
            }
            object=object[key];
        }else{
            object[key]=value
        }
    });
}

class Intent{
    constructor(chat_id){
        this.user_id = chat_id;
        this.setContext(userContextList[chat_id])
    }
    getContext(){
        "use strict";
        return userContextList[this.user_id];
    }
    setContext(context){
        "use strict";
        userContextList[this.user_id] = context;
    }
    setContent(key, content){
        "use strict";
        if(!conversation[this.user_id]){
            conversation[this.user_id] = {};
        }
        setValueToJson(conversation, this.user_id+"."+key, content);
    }
    getContent(key){
        "use strict";
        return getValueFromJSON(conversation, this.user_id+"."+key);
    }
    clearContent(){
        delete conversation[this.user_id];
    }
}

module.exports = {

    id: 'askUser',

    plugin(bot) {

        // On every message
        bot.on('*', (msg, props) => {
            const id = msg.chat.id;
            const context = userContextList[id];
            // If no question, then it's a regular message
            if (!context) return;
            // Delete user from list and send custom event
            delete userContextList[id];
            bot.event('ask.' + context, msg, props, new Intent(context, id));
        });

        // Before call sendMessage method
        bot.on('sendMessage', (args) => {
            const id = args[0];
            const opt = args[2] || {};
            const context = opt.ask;
            // If "ask" in options, add user to list
            if(userContextList[id] !== context){
                (new Intent(context, id)).clearContent();
            }
            if (context) userContextList[id] = context;

        });

    }
};
