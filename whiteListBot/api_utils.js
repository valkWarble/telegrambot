const conf = require("./conf");
const d_conf = require("./dynamic_conf");
let http = require('http');
let https = require('https');
const { URL } = require('url');

/**
 * @class
 * @module
 * @classdesc
 * a utility to make http/https calls
 * */
class API{

    /**
     * @function
     * @param {Object} target - object to be extended
     * @description a utility that merges 2 or more objects
     * */
    _extend(target) {
        let sources = [].slice.call(arguments, 1);
        sources.forEach(function (source) {
            for (let prop in source) {
                if(source.hasOwnProperty(prop)) {
                    target[prop] = source[prop];
                }
            }
        });
        return target;
    }

    /**
     * @function
     * @param {Object} options - query options
     * @description core methods that wil make the actual http/https call
     * */
    query(options){
        if(options.url){
            options.query_options = new URL(options.url);
        }
        let client = http;
        if (options.query_options.protocol && options.query_options.protocol.indexOf("https") === 0){
            client = https;
        }
        if(options.files){
            options.query_options.headers["Content-Type"] = "multipart/form-data";
        }
        let req = client.request(options.query_options, (res) => {
            let output = '';
            res.setEncoding('utf8');
            res.on('data', (chunk) => output += chunk);
            res.on('end', () =>  {
                console.log(">>>>>>>>>>>>>>>>>>>>>>>>query executed successfully<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                console.log(output);
                console.log(">>>>>>>>>>>>>>>>>>>>>>>>query result ^^^ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                options.success(output)
            });
        });
        if(options.files) {
            let form = req.form();
            for(let i=0; i<options.files.length; i++){
                let file_x = options.files[i];
                form.append(file_x.key, file_x.file, file_x.details);
            }
        }
        req.on("error", (e) => {
            console.log("XXXXXXXXX--------unable to execute query---------XXXXXXXXXX");
            console.log(e);
            options.error(e);
        });
        req.write(options.data||"");
        req.end();
    }

    /**
     * @function
     * @description returns default headers
     * */
    _getDefaultHeaders(){
        return conf.partner_api.headers;
    }

    /**
     * @function
     * @param {String} method - http/https method
     * @param {String} path - url path
     * @param {*} data - data to be passed to server
     * @param {Function} on_success - success fn
     * @param {Function} on_error - error fn
     * @param {Object} [options=null] - query options
     * @description calls Query method with query options that will be constructed from conf
     * */
    _send(method, path, data, on_success, on_error, options){
        if(!path.startsWith("/")){
            path = "/"+path;
        }
        let query_options = this._extend({}, {
            path: path,
            method: method.toUpperCase(),
            headers: this._getDefaultHeaders()
        }, d_conf.getHostAndPort(), options);
        if(query_options.headers["Content-Type"] === "application/json"){
            try {
                data = JSON.stringify(data);
            }catch(e){
                console.log("Unable to stringify application/json data", e);
            }
        }
        this.query({
            query_options: query_options,
            data: data,
            success(output){
                if(query_options.headers["Content-Type"] === "application/json"){
                    try {
                        output = JSON.parse(output);
                    }catch(e){
                        console.log("Unable to parse application/json response", e);
                    }
                }
                on_success(output);
            },
            error(e){
                on_error(e);
            }
        });
    }

    /**
     * @function
     * @param {String} path - url path
     * @param {*} data - data to be passed to server
     * @param {Function} on_success - success fn
     * @param {Function} on_error - error fn
     * @param {Object} [options=null] - query options
     * @description posts data to path provided
     * */
    post(path, data, on_success, on_error, options){
        this._send("POST", path, data, on_success, on_error, options);
    }

    /**
     * @function
     * @param {String} path - url path
     * @param {*} data - data to be passed to server
     * @param {Function} on_success - success fn
     * @param {Function} on_error - error fn
     * @param {Object} [options=null] - query options
     * @description queries a GET response from path provided
     * */
    get(path, data, on_success, on_error, options){
        this._send("GET", path, data, on_success, on_error, options);
    }
}
module.exports = API;