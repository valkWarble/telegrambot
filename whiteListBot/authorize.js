const conf = require("./conf");
const d_conf= require("./dynamic_conf");
let crypto = require("crypto");

/**
 * @module
 * @description
 * Utility to authorize an admin.
 * Please run `AuthUtils.generate_salted_hash(admin_telegrram_username, new_passphrase)` and paste the return value in the config under `conf.auth.salted_hash`
 * */
let AuthUtils = {

    /**
     * @function
     * @param {String} user - username of the user
     * @param {String} pass_phrase - pass phrase for the admin
     * @returns {String} salted hash of the string. May change the salt from app to app.
     * */
    generate_salted_hash(user, pass_phrase){
        let salt = d_conf.getConf(conf.env_keys.authorization.salt);
        let hash = crypto.createHmac('sha512', salt);
        hash.update(user+"::"+pass_phrase);
        return hash.digest('hex');
    },

    /**
     * @function
     * @param {String} user - username of the user
     * @param {String} pass_phrase - pass phrase for the admin
     * @returns {Boolean} If user is authenticated
     * */
    authorize(user, pass_phrase){
        "use strict";
        //TODO !!!!!!!!!!!!!!!!!!!IMPORTANT!!!!!!!!!!!!!!!!!!!
        //TODO modify it to have user+pass_phrase
        //TODO modify it to have user+pass_phrase
        //TODO modify it to have user+pass_phrase
        //TODO modify it to have user+pass_phrase
        //TODO modify it to have user+pass_phrase
        return d_conf.getConf(conf.env_keys.authorization.salt) === AuthUtils.generate_salted_hash(0, pass_phrase);
    }
};

module.exports = AuthUtils;