module.exports = {
    env_keys : {
        bot: {
            token: "TELEGRAM_BOT_TOKEN",
            name: "TELEGRAM_BOT_NAME",
        },
        authorization: {
            salt: "TELEGRAM_AUTHORIZATION_SALT",
            salted_hash: "TELEGRAM_AUTHORIZATION_SALTED_HASH"
        },
        partner_api: {
            host: "PARTNER_API_HOST",
            port: "PARTNER_API_PORT",
            custom: {
                host: "PARTNER_API_{0}_HOST",
                port: "PARTNER_API_{0}_PORT"
            }
        }
    },
    bot: {
        // name : "eximchaintihor_bot",
        // token: "468985617:AAHNviJtNVyi0JmGV6WU-FscGz4WndSoTY0",
        // token: "494965164:AAGN5b4rlUpMNiH7nDH98TzkUMwNkn7vigo",
        // name : "eximchainDEVONLYBOT",
        commands: {
            addAddress: "joinwhitelist",
            whitelistAddress: "approvewhitelist",
            isWhitelisted: "checkwhitelist",
            submitInformation: "submitinformation",
            submitDocument: "submitdocument",
            checkStatus: "checkstatus"
        },
        pluginFolder: "telebot_plugins"
    },
    // auth: {
    //     salted_hash: '95661925b4cb97b978fce56331aeaafe001a400e59d1d411c2599f193dc6d2c5c587c02beb238604a377155f8078f0b25b53ce603fffc041e3183311581563df'
    // },
    ethereum: {
        provider: "http://localhost:8545",
        whitelist_contract : {
            build_path : "../whiteListContracts/build/contracts/Whitelist.json",
            contract_address : "0x5d98006cc42a122e5276dd338703dc48f9baa736", //"0x76244ade65f2dc3f3987fa6e0b76546470f9930c",
            owner_address : "0xd71bca4d2989738023689de1a2d1d9407752d31e"    //"0x1c013fec2b4fa0e65c560ea7f7e533e5c3666998"
            // contract_address : "0x76244ade65f2dc3f3987fa6e0b76546470f9930c",
            // owner_address : "0x1c013fec2b4fa0e65c560ea7f7e533e5c3666998"
        }

    },
    partner_api: {
        headers: {
            "Content-Type" :"application/json",
            // "WEB2PY-USER-TOKEN":"_YOUR_TOKEN"
        },
        operations: {
            checkStatus: {
                path: "/default/check_status.json/"
            },
            submitInformation: {
                path: "/default/individual_risk/"
            },
            submitDocument: {
                options: {
                    headers : {
                        "Content-Type" :"multipart/form-data"
                    }
                },
                path: "/default/individual_file_upload/"
            }
        },
        validation: {
            submitDocument: {
                mime_type_contains: ["pdf", "image"]
            },
            submitInformation: {
                fields: [
                    {
                        name: "first_name",
                        type: "strict_string"
                    },
                    {
                        name: "last_name",
                        type: "strict_string"
                    },
                    {
                        name: "country_of_residence",
                        type: "choice",
                        enum: {
                            file: "./data/_country.csv",
                            inline_query_command: "country"
                        }
                    },
                    {
                        name: "nationality",
                        type: "choice",
                        enum: {
                            file: "./data/_nationality.csv",
                            inline_query_command: "nationality"
                        }
                    },
                    {
                        name: "ssic_code",
                        type: "choice",
                        enum: ["46512 - WHOLESALE OF COMPUTER SOFTWARE (EXCEPT GAMES)"]
                    },
                    {
                        name: "ssoc_code",
                        type: "choice",
                        enum: ["13302 - SOFTWARE AND APPLICATIONS MANAGER"]
                    },
                    {
                        name: "onboarding_mode",
                        type: "choice",
                        enum: ["UNKNOWN", "FACE-TO-FACE", "NON FACE-TO-FACE"]
                    },
                    {
                        name: "payment_mode",
                        type: "choice",
                        enum: ["UNKNOWN", "TELEGRAPHIC TRANSFER", "CHEQUE (LOCAL)",
                            "CHEQUE (FOREIGN)", "CREDIT CARD", "VIRTUAL CURRENCY",
                            "CASH"]
                    },
                    {
                        name: "product_service_complexity",
                        type: "choice",
                        enum: ["UNKNOWN", "SIMPLE", "COMPLEX", "HIGH RISK", "UNUSUAL"]
                    }
                ]
            }
        }
    }
};