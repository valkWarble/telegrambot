const API = require('./api_utils');
const conf = require('./conf');
const d_conf = require('./dynamic_conf');
const fs = require("fs");
const parse = require('csv-parse');

/**
 * @class
 * @classdesc
 * partner connector interacts with the API Utils and submits information or retrieves information from Partner API Service
 * */
class PartnerConnector{

    /**
     * @constructor
     * @description
     * creates a new API utils instance
     * */
    constructor(){
        this.API = new API();
    }

    /**
     * @function
     * @param {Number} rfrId - rfr id to be passed in data
     * @param {String} fileName - name of file
     * @param {String} fileType - mime type
     * @param {Stream} document - document to be uploaded as multipart
     * @param {Function} on_success - callback on successful query execution. For api error, this function will be called
     * @param {Function} on_error - callback on failure of query execution, internally
     * @description calls upload doc api
     * */
    uploadDocument(rfrId, fileName, fileType, document, on_success, on_error){
        let o = this.API._extend({}, conf.partner_api.operations.submitDocument.options, {
            file : [{
                key: "file",
                file: document,
                details: {
                    // filename: 'myfile.txt',
                    contentType: fileType,
                    rfrID: rfrId,
                    fileName: fileName,
                }
            }]
        }, d_conf.getHostAndPort("submitDocument"));
        this.API.post(conf.partner_api.operations.submitDocument.path, null, on_success, on_error, o);
    }

    /**
     * @function
     * @param {Number} rfrId - rfr id to be passed in data
     * @param {Function} on_success - callback on successful query execution. For api error, this function will be called
     * @param {Function} on_error - callback on failure of query execution, internally
     * @description calls check status api
     * */
    checkStatus(rfrId, on_success, on_error){
        this.API.get(conf.partner_api.operations.checkStatus.path, {
            rfrID: rfrId
        }, on_success, on_error);
    }

    /**
     * @function
     * @param {Object} data - user info data
     * @param {Function} on_success - callback on successful query execution. For api error, this function will be called
     * @param {Function} on_error - callback on failure of query execution, internally
     * @description calls submit info api
     * */
    submitInformation(data, on_success, on_error){
        this.API.post(conf.partner_api.operations.submitInformation.path, data, on_success, on_error);
    }

}

/**
 * @class
 * @classdesc
 * API error class will be used by the validator
 * */
class APIError extends Error{
    constructor(message, extra){
        "use strict";
        super(message, extra);
    }
}

/**
 * @member {Object<String, Function>}
 * @description
 * validation methods are packed into validator as a utility
 * */
const Validator = {

    /**
     * @function
     * @param {Object} field - field object from configuration
     * @param {*} value - field value
     * @param {Function}  callback - to be called back upon validation
     * @description
     * validates for a strict string. will pass an APIError instance in case of mismatch
     * */
    _strict_string(field, value, callback){
        "use strict";
        if(typeof value === "string" && !isNaN(+value)){
            return callback(new APIError("info.invalid_input.string"));
        }
        return callback()
    },

    /**
     * @function
     * @param {Object} field - field object from configuration
     * @param {*} value - field value
     * @param {Function}  callback - to be called back upon validation
     * @description
     * validates for existence if choices. will pass an APIError instance in case of mismatch
     * */
    _choice(field, value, callback){
        "use strict";
        Configurations.getEnumValues(field, (enum_values) => {
            let idx = enum_values.map( ev => ev.toUpperCase()).indexOf(value.toUpperCase());
            if(idx === -1){
                if(field.enum.file){
                    return callback(new APIError("info.invalid_input.choice.inline_query"));
                }
                return callback(new APIError("info.invalid_input.choice"));
            }
            return callback(undefined, enum_values[idx]);
        });
    },

    /**
     * @function
     * @param {Object} field - field object from configuration
     * @param {*} value - field value
     * @param {Function}  callback - to be called back upon validation
     * @description
     * call specific validators based on field type
     * */
    validate(field, value, callback){
        "use strict";
        Validator["_"+field.type](field, value, callback);
    }
};

/**
 * @member {Object<String, Function>}
 * @description
 * Configuration utility
 * */
const Configurations = {

    /**
     *  @member {Object<String, Array<Object<String, String>>>}
     *  @description stores a list of values for a field which are to be loaded from files...
     * */
    _enum_values_from_file : {},

    /**
     * @function
     * @param {Object} field - field object from configuration
     * @param {Function}  callback - to be called back upon validation
     * @description
     * load's values from csv and passes them to callback function
     * */
    _loadFromCSV(field, callback){
        "use strict";
        if(Configurations._enum_values_from_file[field.enum.file]){
            return callback(Configurations._enum_values_from_file[field.enum.file]);
        }
        fs.readFile(field.enum.file, function (err, fileData) {
            if(err){
                //TODO error stuff
                console.log(err);
            }else {
                parse(fileData, {}, function (err, rows) {
                    if(rows[0].length === 1) {
                        rows = rows.map(row => row[0]);
                    }
                    Configurations._enum_values_from_file[field.enum.file] = rows;
                    callback(rows);
                });
            }
        })
    },

    /**
     * @function
     * @param {Object} field - field object from configuration
     * @param {Function}  callback - to be called back upon validation
     * @description
     * load's enum values and passes them to callback
     * */
    getEnumValues(field, callback){
        if(field.enum instanceof Array){
            callback(field.enum);
        }else{
            Configurations._loadFromCSV(field, callback);
        }
    }

};

PartnerConnector.APIError = APIError;
PartnerConnector.Validator = Validator;
PartnerConnector.Configurations = Configurations;
module.exports = PartnerConnector;