const API = require('./api_utils');
const conf = require("./conf");
const AuthUtils = require("./authorize");
const i18n = require("./i18n");
const ETHInterface = require('./eth.js');
const partnerAPI = require('./partner_connector');
const d_conf = require('./dynamic_conf');


/**
 * @class
 * @classdesc
 * all bot event handlers will be defined here
 * */
class BotUtils{

    /**
     * @constructor
     * @param {Object} bot - isntance of telebot
     * @description
     * instantiates all dependencies
     * */
    constructor(bot){
        this.ETH = new ETHInterface.ETH();
        this.PartnerAPI = new partnerAPI();
        this.API = new API();
        this.bot = bot;
    }

    /**
     * @function
     * @private
     * @param {String} cmd - text msg that user has send to the bot
     * @returns {Array<String>} command params in an array
     * */
    _getCommandParams(cmd) {
        "use strict";
        return cmd.split(" ").splice(1);
    }

    /**
     * @function
     * @private
     * @param {Error} e - Error instance
     * @returns {String} reply message extracted from error or a generic message
     * */
    _handleError(e) {
        "use strict";
        if (e instanceof ETHInterface.ETHError) {
            return i18n(e.message);
        }else if (e instanceof partnerAPI.APIError) {
            return i18n(e.message);
        } else {
            console.log(e);
            return i18n("generic.error");
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * command handler for add-address
     * */
    onAddAddress_cmd(msg){
        "use strict";
        try {
            let address = this._getCommandParams(msg.text)[0];
            if(address){
                msg.text = address;
                this.onAddAddress_address(msg);
            }else{
                msg.reply.text(i18n("address.for.add"), {ask: BotUtils.commands.ADD_ADDRESS});
            }
        } catch (e) {
            msg.reply.text(this._handleError(e));
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from telebot
     * @description
     * command continuation for onAddAddress_cmd. when requested for user's eth address this will be called
     * */
    onAddAddress_address(msg){
        "use strict";
        try{
            let address = msg.text.trim();
            this.ETH.addAddress(address, (data) => {
                //TODO handle data
                msg.reply.text(i18n("address.added", [address]));
            });
        } catch (e) {
            msg.reply.text(this._handleError(e), {ask: BotUtils.commands.ADD_ADDRESS});
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * command handler for whitelist-address
     * */
    onWhitelistAddress_cmd(msg){
        "use strict";
        try{
            msg.reply.text(i18n("authenticate.provide.pass_phrase"), {ask: BotUtils.commands.WHITELIST_ADDRESS+"_AUTH"});
        } catch (e) {
            msg.reply.text(this._handleError(e));
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from telebot
     * @description
     * command continuation for onWhitelistAddress_cmd for authentication
     * */
    onWhitelistAddress_auth(msg){
        try{
            if(AuthUtils.authorize(msg.from.username, msg.text)){
                msg.reply.text(i18n("authentication.success")+" "+i18n("address.for.approve"), {ask: BotUtils.commands.WHITELIST_ADDRESS});
            }else{
                msg.reply.text(i18n("authentication.failed"), {ask: BotUtils.commands.WHITELIST_ADDRESS+"_AUTH"});
            }
        }catch(e){
            msg.reply.text(this._handleError(e), {ask: BotUtils.commands.WHITELIST_ADDRESS+"_AUTH"});
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from telebot
     * @description
     * command continuation for onWhitelistAddress_auth for address to be whitelsited
     * */
    onWhitelistAddress_address(msg){
        try {
            let address = msg.text.trim();
            this.ETH.whitelistAddress(address, (data) => {
                //TODO handle data
                msg.reply.text(i18n('address.whitelisting.success', [address])+"\n"+i18n("recommend.message.delete"));
            });
        } catch (e) {
            msg.reply.text(this._handleError(e), {ask: BotUtils.commands.WHITELIST_ADDRESS});
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * command handler for check-address-inwhitelist
     * */
    onCheckAddress_cmd(msg){
        "use strict";
        try {
            let address = this._getCommandParams(msg.text)[0];
            if(address){
                msg.text = address;
                this.onCheckAddress_address(msg);
            }else{
                msg.reply.text(i18n("address.for.check"), {ask: BotUtils.commands.IS_WHITELISTED});
            }
        } catch (e) {
            msg.reply.text(this._handleError(e));
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from telebot
     * @description
     * command continuation for onCheckAddress_cmd to get the address to be checked for
     * */
    onCheckAddress_address(msg){
        "use strict";
        try{
            let address = msg.text.trim();
            this.ETH.isWhitelisted(address, (data) => {
                if(data===true){
                    data = i18n("address.check.true", [address]);
                }else{
                    data = i18n("address.check.false", [address]);
                }
                msg.reply.text(data);
            });
        } catch (e) {
            msg.reply.text(this._handleError(e), {ask: BotUtils.commands.IS_WHITELISTED});
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from telebot
     * @returns {Number} chat id as rfrID
     * */
    _getRfrId(msg){
        return msg.from.id;
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * command handler for submitting a document
     * */
    onSubmitDocument_cmd(msg){
        "use strict";
        try{
            msg.reply.text(i18n("upload.document"), {ask: BotUtils.commands.SUBMIT_DOCUMENT});
        }catch(e){
            msg.reply.text(this._handleError(e));
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * check whether a submitted doc is in the required format or not
     * */
    _isRequiredDocument(msg){
        let allowed_mime_type_contains = conf.partner_api.validation.submitDocument.mime_type_contains;
        let allowed = false;
        for(let i=0; i<allowed_mime_type_contains.length; i++){
            if(msg.document.mime_type.indexOf(allowed_mime_type_contains[i]) !== -1){
                allowed = true;
                break;
            }
        }
        return allowed && !!msg.document;
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from telebot
     * @description
     * command continuation for onSubmitDocument_cmd to get the user's uploaded document
     * */
    onSubmitDocument_conversation(msg){
        "use strict";
        try{
            if(this._isRequiredDocument(msg)){
                /**
                 * gets the file from telegram
                 * */
                this.bot.getFile(msg.document.file_id).then(file_details => {
                    let self = this;
                    this.API.query({
                        url: file_details.fileLink,
                        success(output){
                            /**
                             * on successful download of file stream, will be forwarded to partner api
                             * */
                            self.PartnerAPI.uploadDocument(
                                self._getRfrId(msg),
                                msg.document.file_name,
                                msg.document.mime_type,
                                output,
                                () => {
                                    msg.reply.text(i18n("document.uploaded"));
                                },
                                () => {
                                    msg.reply.text(i18n("api.failure.upload_document"));
                                }
                            );
                        },
                        error(e){
                            msg.reply.text(i18n("generic.error"));
                            console.log("Unable to get file from telegram!!!"+e.message);
                        }
                    });
                });
            }else {
                msg.reply.text(i18n("invalid.document"), {ask: BotUtils.commands.SUBMIT_DOCUMENT});
            }
        }catch(e){
            msg.reply.text(this._handleError(e), {ask: BotUtils.commands.SUBMIT_DOCUMENT});
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * command handler for status check from partner api
     * */
    onCheckStatus_cmd(msg){
        "use strict";
        try {
            this.PartnerAPI.checkStatus(this._getRfrId(msg), (response) => {
                let approval_status = response['approval_status'];
                if(approval_status) {
                    msg.reply.text(i18n("user.approval.status", [approval_status]));
                }else{
                    msg.reply.text(i18n("user.approval.not_submitted", [approval_status]));
                }
            }, (error) => {
                msg.reply.text(i18n("api.failure.check_status"));
            });
        } catch (e) {
            msg.reply.text(this._handleError(e));
        }
    }

    /**
     * @function
     * @private
     * @param {Number} index - required field from index
     * @returns {Object} field from the conf based on its index from the conf
     * */
    _getInformationField(index){
        return conf.partner_api.validation.submitInformation.fields[index];
    }

    /**
     * @function
     * @private
     * @param {Object} field - field object from configuration
     * @returns {String} question text for provided field
     * */
    _getInformationQuery(field){
        return i18n("info.questionnaire."+field.name)
            + ( (field.enum && field.enum.file)
                    ? "\n"+i18n("info.enum.inline_query", [
                            i18n("info.field."+field.name),
                            d_conf.getConf(conf.env_keys.bot.name),
                            field.enum.inline_query_command])
                    : "" );
    }

    /**
     * @function
     * @private
     * @param {Number} index - current question index
     * @description
     * checks the index against the questions length from configuration
     * @returns {Boolean}
     * */
    _isInformationQueryComplete(index){
        return conf.partner_api.validation.submitInformation.fields.length-1 === index;
    }

    /**
     * @function
     * @private
     * @param {Object} field - field object from configuration
     * @param {Object} msg - msg object from the telebot
     * @description
     * triggers a response to the user with next question in stack from the conf
     * */
    _ask_info_question(field, msg){
        /**
         * Adding reply markup, inline buttons in case it is a choice field!!!
         * */
        if(field.type === "choice" && field.enum instanceof Array) {
            let inlineButtons = field.enum.map(enum_value => {
                return this.bot.inlineButton(enum_value, {callback: enum_value});
            });
            let counter = 0;
            let inlineKeyboardButtons = [];
            while (inlineButtons.length) {
                let row = Math.floor(counter / 2);
                if (!inlineKeyboardButtons[row]) {
                    inlineKeyboardButtons[row] = [];
                }
                inlineKeyboardButtons[row][counter % 2] = inlineButtons.shift();
                counter++;
            }
            let markup = this.bot.inlineKeyboard(inlineKeyboardButtons, {resize: true});
            this.bot.sendMessage(msg.from.id, this._getInformationQuery(field), {
                markup,
                ask: BotUtils.commands.SUBMIT_INFO
            });
        }else{
            this.bot.sendMessage(msg.from.id, this._getInformationQuery(field), {ask: BotUtils.commands.SUBMIT_INFO});
        }
    }


    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * command handler for submitting user info
     * */
    onSubmitInformation_cmd(msg){
        "use strict";
        try{
            this._ask_info_question(this._getInformationField(0), msg);
        }catch(e){
            msg.reply.text(this._handleError(e));
        }
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * command handler to continue user conversation
     * */
    onSubmitInformation_conversation(msg){
        "use strict";
        try{
            /**
             * Getting current question index form intent's content
             * */
            let intent = msg.intent;
            let current_question = 0;
            if(intent.hasContent()){
                current_question = intent.getContent("current_question");
            }else{
                intent.setContent("current_question", 0);
            }
            let field = this._getInformationField(current_question);
            let field_value = null;
            if(field.type === "choice" && !field.enum.file){
                field_value = msg.data;
            }else{
                field_value = msg.text.trim();
            }
            try {
                /**
                 * Validating the user input value
                 * */
                partnerAPI.Validator.validate(field, field_value, (err, modified_field_value) => {
                    if(!err){
                        /**
                         * Updating current question data in intent's content!
                         * */
                        intent.setContent("data." + field.name, modified_field_value || field_value);
                        /**
                         * Calling partner api on gethering all the required information!
                         * */
                        if(this._isInformationQueryComplete(current_question)){
                            this._submitInformation_conversationComplete(intent.getContent("data"), msg);
                            return;
                        }
                        /**
                         * Updating current question index
                         * */
                        current_question++;
                        /**
                         * send next message
                         * */
                        this._ask_info_question(this._getInformationField(current_question), msg);
                        intent.setContent("current_question", current_question);
                    }else{
                        this.bot.sendMessage(msg.from.id, this._handleError(err), {ask: BotUtils.commands.SUBMIT_INFO});
                    }
                });
            }catch (e){
                this.bot.sendMessage(msg.from.id, this._handleError(e), {ask: BotUtils.commands.SUBMIT_INFO});
            }
        }catch(e){
            this.bot.sendMessage(msg.from.id, this._handleError(e), {ask: BotUtils.commands.SUBMIT_INFO});
        }
    }

    /**
     * @function
     * @private
     * @param {Object} data - data to be posted to partner api for submit information command
     * @param {Object} msg - msg object from the telebot
     * @description
     * initiates posting information to PARTNER API
     * */
    _submitInformation_conversationComplete(data, msg){
        data.rfrID = this._getRfrId(msg);
        this.PartnerAPI.submitInformation(data, (response) => {
            /**
             * success response:: `{ "rfrID":"prod0001", "status": "COMPLETE", "approval_status": "CLEARED" }`
             * error response :: `{ "rfrID":"prod0002", "errors": { "ssic_code": "Value not allowed", "ssoc_code": "Value not allowed" }, "id": null }`
             * */
            if(response.errors){
                let response_error_messages = [];
                for(let k in response.errors){
                    if(response.errors.hasOwnProperty(k)) {
                        response_error_messages = i18n("info.invalid_input.http_error", [response.errors[k], k]);
                    }
                }
                this.bot.sendMessage(msg.from.id, response_error_messages.join("\n"));
            }else {
                this.bot.sendMessage(msg.from.id, i18n("information.submitted", [response.approval_status, conf.bot.commands.checkStatus]));
            }
        }, (error) => {
            this.bot.sendMessage(msg.from.id, i18n("api.failure.upload_individual"));
        });
    }

    /**
     * @function
     * @private
     * @param {Object} msg - msg object from the telebot
     * @description
     * Inline query handler for `@botname_bot country india` kind of queries which returns values from `.csv` or so...
     * */
    onInlineQuery(msg){
        let query = msg.query;
        // Create a new answer list object
        const answers = this.bot.answerList(msg.id, {cacheTime: 3600});
        let splits = query.split(" ");
        if(splits[0]){
            let field = conf.partner_api.validation.submitInformation.fields.filter( field => {
                return field.type === "choice" && !(field.enum instanceof Array) && field.enum.inline_query_command === splits[0];
            })[0];
            partnerAPI.Configurations.getEnumValues(field, (enum_values) => {
                let search_str = splits[1];
                if(search_str) {
                    enum_values = enum_values.filter(enum_value => enum_value.indexOf(search_str.toUpperCase()) !== -1);
                    let unique_values = {};
                    enum_values.forEach(enum_value => unique_values[enum_value] = null);
                    enum_values = Object.keys(unique_values);
                    enum_values.forEach(enum_value => {
                        answers.addArticle({
                            id: enum_value,
                            title: enum_value,
                            message_text: enum_value
                        });
                    });
                }
                // Send answers
                return this.bot.answerQuery(answers);
            });
        }

    };

}

/**
 * internal reference to the bot commands...
 * */
BotUtils.commands = {
    ADD_ADDRESS : conf.bot.commands.addAddress,
    WHITELIST_ADDRESS : conf.bot.commands.whitelistAddress,
    IS_WHITELISTED : conf.bot.commands.isWhitelisted,
    SUBMIT_INFO : conf.bot.commands.submitInformation,
    SUBMIT_DOCUMENT : conf.bot.commands.submitDocument,
    CHECK_STATUS : conf.bot.commands.checkStatus
};

module.exports = BotUtils;