const TelegramBot = require('telebot');
const conf = require('./conf');
const d_conf = require('./dynamic_conf');
const BotUtils = require("./bot_utils");


let path = require('path');

process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
});

/**
 * @class
 * @classdesc creates a bot instance, attaches listeners and starts the bot
 * */
class BotHandler{

    /**
     * @constructor
     * @description
     * creates telebot instance in polling mode
     * also creates a bot utils instance
     * */
    constructor(){
        "use strict";
        this.bot = new TelegramBot({
            token: d_conf.getConf(conf.env_keys.bot.token),
            usePlugins: ['chatWithUser'],    //Helpful in chatting with user by capturing user's intent
            pluginFolder: __dirname+path.sep+conf.bot.pluginFolder+path.sep,
            polling: {
                interval: 500, // Optional. How often check updates (in ms).
                timeout: 0, // Optional. Update polling timeout (0 - short polling).
                limit: 100, // Optional. Limits the number of updates to be retrieved.
                retryTimeout: 5000 // Optional. Reconnecting timeout (in ms).
            },
        });
        this.utils = new BotUtils(this.bot);
    };

    /**
     * @function
     * @description
     * attaches events to the bot
     * Here, commands as well as conversations using askUser plugin
     * */
    attachListeners(){
        "use strict";
        this.bot.on(['/start', '/hello'], (msg) => msg.reply.text('Welcome!'));
        //takes an ethereum address as an input and adds it as a key to the address=>bool mapping called userAddr
        // with value false

        //----------------------------Commands to handle ETH Contract------------------------------------------

        this.bot.on('/'+BotUtils.commands.ADD_ADDRESS, (msg) => this.utils.onAddAddress_cmd(msg));
        this.bot.on('ask.'+BotUtils.commands.ADD_ADDRESS, (msg) => this.utils.onAddAddress_address(msg));

        //takes an ethereum address as an input to access the address=>bool mapping called userAddr setting the
        // value of userAddr[user] to true
        this.bot.on('/'+BotUtils.commands.WHITELIST_ADDRESS, (msg) => this.utils.onWhitelistAddress_cmd(msg));
        this.bot.on('ask.'+BotUtils.commands.WHITELIST_ADDRESS+"_AUTH", (msg) => this.utils.onWhitelistAddress_auth(msg));
        this.bot.on('ask.'+BotUtils.commands.WHITELIST_ADDRESS, (msg) => this.utils.onWhitelistAddress_address(msg));

        //takes an ethereum address as an input to access the address=>bool mapping called userAddr and returns
        // the bool value of the matching key
        this.bot.on('/'+BotUtils.commands.IS_WHITELISTED, (msg) => this.utils.onCheckAddress_cmd(msg));
        this.bot.on('ask.'+BotUtils.commands.IS_WHITELISTED, (msg) => this.utils.onCheckAddress_address(msg));

        //----------------------------Commands to handle Partner APIs------------------------------------------
        //Submits a users details
        this.bot.on("/"+BotUtils.commands.SUBMIT_INFO, (msg) => this.utils.onSubmitInformation_cmd(msg));
        this.bot.on("ask."+BotUtils.commands.SUBMIT_INFO, (msg) => this.utils.onSubmitInformation_conversation(msg));

        //Submits a users document
        this.bot.on("/"+BotUtils.commands.SUBMIT_DOCUMENT, (msg) => this.utils.onSubmitDocument_cmd(msg));
        this.bot.on("ask."+BotUtils.commands.SUBMIT_DOCUMENT, (msg) => this.utils.onSubmitDocument_conversation(msg));
        // this.bot.on("document", (msg) => this.utils.onSubmitDocument_conversation(msg));

        //Check's a user's document status
        this.bot.on("/"+BotUtils.commands.CHECK_STATUS, (msg) => this.utils.onCheckStatus_cmd(msg));
        this.bot.on('inlineQuery', msg => this.utils.onInlineQuery(msg));
    }

    /**
     * @function
     * @description
     * activates the bot
     * */
    listen(){
        "use strict";
        this.bot.start();
    }

}

// creating a bot handler instance attaching listeners and start listening...
let bot_handler = new BotHandler();
bot_handler.attachListeners();
bot_handler.listen();