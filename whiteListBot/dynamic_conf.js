let env = process.env;
const conf = require('./conf');

module.exports = {

    getConf(name){
        "use strict";
        return env[name];
    },
    getHostAndPort(command){
        "use strict";
        if(command) {
            let c_host = getConf( conf.env_keys.partner_api.custom.host.replace( new RegExp( "\\{0\\}","g") , command) );
            let c_port = getConf( conf.env_keys.partner_api.custom.port.replace( new RegExp( "\\{0\\}","g") , command) );
            if(!c_host) {
                c_host = getConf("PARTNER_API_HOST");
            }
            if(!c_port){
                c_port = getConf("PARTNER_API_PORT");
            }
            return { host: c_host, port: c_port }
        }else{
            return {
                host: getConf("PARTNER_API_HOST"),
                port: getConf("PARTNER_API_PORT")
            }
        }
    }

};