'use strict';
const assert = require('chai').assert;
const AuthUtils = require('../authorize');
const conf = require('../conf');

describe('AuthUtils', () => {
    describe('"Authorize"', () => {
        process.env[conf.env_keys.authorization.salted_hash] = AuthUtils.generate_salted_hash("username", "passphrase");
        let authenticated = AuthUtils.authorize("username", "passphrase");
        let false_authentication = AuthUtils.authorize("username", "xpassphrase");
        it('Authentication success for username and passphrase', () => {
            assert(authenticated);
        });
        it('Authentication should fail for username and xpassphrase', () => {
            assert(!false_authentication);
        });
    })
});