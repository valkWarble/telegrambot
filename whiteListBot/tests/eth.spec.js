'use strict';
const assert = require('chai').assert;
const ETH_module = require('../eth');

describe('ETH', () => {
    let ETH = new ETH_module.ETH();
    describe('"_ethAddressValidityCheck"', () => {
        let valid_eth_address = "0xa8cc50dbc20ffe228a02779cafbd30ca492f33f2";
        let invalid_eth_address = "0xa8cc50dbc20ffe228a02779cafbd30ca492f33x2";
        let invalid_eth_address2 = "0xa8cc50dbc20ffe228a02779cafbd30ca492f33f2a";
        it('Should pass validation', () => {
            assert(ETH._ethAddressValidityCheck(valid_eth_address))
        });
        it('Should throw error for invalid address', () => {
            assert.throws(() => ETH._ethAddressValidityCheck(invalid_eth_address), ETH.ETHError);
        });
        it('Should throw error for invalid address 2', () => {
            assert.throws(() => ETH._ethAddressValidityCheck(invalid_eth_address2), ETH.ETHError);
        });
    })
});