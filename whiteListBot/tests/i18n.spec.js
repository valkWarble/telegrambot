'use strict';
const assert = require('chai').assert;
const i18n = require('../i18n');


describe('I18N', () => {
    describe('"i18n"', () => {
        it('Should return "test key one"', () => {
            assert(i18n("test.key.1") === "test key one")
        });
        it('Should return "test key XYZw"', () => {
            assert(i18n("test.key.hollow", ["XYZw"]) === "test key XYZw")
        });
    });
});